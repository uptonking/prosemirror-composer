# Prosemirror Composer

A useful way to keep complex prosemirror projects tidy, maintainable and testable.

## Install

```
npm install prosemirror-composer -S
```

## Example

```ts
import { EditorView } from 'prosemirror-view';
import { TextSelection } from 'prosemirror-state';
import { composeModifiers, insertText, setSelection } from 'prosemirror-composer';

function handleInsertWordAndSelect(view: EditorView, text: string) {
  const transaction = view.state.tr;
  const { selection, doc } = transaction;

  const nextTransaction = composeModifiers([
    insertText(text, selection.from, selection.to),
    setSelection(
      new TextSelection(
        transaction.doc.resolve(selection.from),
        transaction.doc.resolve(selection.from + text.length),
      )
    ),
  ])(transaction);

  dispatchTransaction(nextTransaction);
}
```
