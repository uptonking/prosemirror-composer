import { Transaction, Selection, PluginKey, Plugin } from 'prosemirror-state';
import { NodeType, Mark, Node, Fragment, Slice } from 'prosemirror-model';

interface Attrs {
  [key: string]: any;
}

/*
  Default Transaction Modifiers
  http://prosemirror.net/docs/ref/#transform.Transform
*/

export const setSelection = (selection: Selection) => (
  transaction: Transaction,
): Transaction => transaction.setSelection(selection);

export const setStoredMarks = (marks: Mark[]) => (
  transaction: Transaction,
): Transaction => transaction.setStoredMarks(marks);

export const setTime = (time: number) => (
  transaction: Transaction,
): Transaction => transaction.setTime(time);

export const replaceWith = (
  from: number,
  to: number,
  content: Fragment | Node | [Node],
) => (transaction: Transaction): Transaction =>
  transaction.replaceWith(from, to, content);

export const replaceSelection = (slice: Slice) => (
  transaction: Transaction,
): Transaction => transaction.replaceSelection(slice);

export const replaceRange = (from: number, to: number, slice: Slice) => (
  transaction: Transaction,
): Transaction => transaction.replaceRange(from, to, slice);

export const replaceSelectionWith = (node: Node, inheritMarks?: boolean) => (
  transaction: Transaction,
): Transaction => transaction.replaceSelectionWith(node, inheritMarks);

export const deleteSelection = () => (transaction: Transaction): Transaction =>
  transaction.deleteSelection();

export const insertText = (text: string, from?: number, to?: number) => (
  transaction: Transaction,
): Transaction => transaction.insertText(text, from, to);

export const setMeta = (
  key: string | Plugin | PluginKey,
  value: boolean | number | string,
) => (transaction: Transaction): Transaction => transaction.setMeta(key, value);

export const setNodeMarkup = (
  pos: number,
  type?: NodeType,
  attrs?: Attrs,
  marks?: Mark[],
) => (transaction: Transaction): Transaction =>
  transaction.setNodeMarkup(pos, type, attrs, marks);

type TypeAfter = { type: NodeType; attrs?: Attrs };

export const split = (pos: number, depth = 1, typesAfter?: TypeAfter[]) => (
  transaction: Transaction,
): Transaction => transaction.split(pos, depth, typesAfter);
