import { Transaction } from 'prosemirror-state';

type ModifierFn = (tr: Transaction) => Transaction;

export const composeModifiers = (modifiers: ModifierFn[] = []) => (
  transaction: Transaction,
) => {
  return modifiers.reduce((curr, next) => next(curr), transaction);
};

